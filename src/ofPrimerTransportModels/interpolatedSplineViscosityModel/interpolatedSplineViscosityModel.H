/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2011 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::viscosityModels::interpolatedSplineViscosityModel

Description
     A viscosity model which takes in raw rheometry data and 
        uses splines to compute a local effective viscosity based
        on the data table.  

SourceFiles
    interpolatedSplineViscosityModel.C

\*---------------------------------------------------------------------------*/

#ifndef interpolatedSplineViscosityModel_H
#define interpolatedSplineViscosityModel_H

#include "viscosityModel.H"
#include "dimensionedScalar.H"
#include "volFields.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{
namespace viscosityModels
{

/*---------------------------------------------------------------------------*\
                           Class interpolatedSplineViscosityModel Declaration
\*---------------------------------------------------------------------------*/

class interpolatedSplineViscosityModel
:
    public viscosityModel
{
    // Private data

        //Dictionary for the viscosity model
        dictionary modelDict_;

        //x and y entries of the rheology data for
        //   use with the spline interpolator
        fileName dataFileName_;
        scalarField rheologyTableX_;
        scalarField rheologyTableY_;

        tmp<volScalarField> nuPtr_;

    // Private Member Functions

        // Load the viscosity data table
        void loadViscosityTable();

public:

    //- Runtime type information
    TypeName("interpolatedSplineViscosityModel");


    // Constructors

        //- Construct from components
        interpolatedSplineViscosityModel
        (
            const word& name,
            const dictionary& viscosityProperties,
            const volVectorField& U,
            const surfaceScalarField& phi
        );


    //- Destructor
    ~interpolatedSplineViscosityModel()
    {}


    // Member Functions

        //- Return the laminar viscosity
        tmp<volScalarField> nu() const
        {
            return nuPtr_;
        }

        //- Return the laminar viscosity for patch
        virtual tmp<scalarField> nu(const label patchi) const
        {
            return nuPtr_->boundaryField()[patchi];
        }

        //- Correct the laminar viscosity
        virtual void correct();

        //- Read transportProperties dictionary
        bool read(const dictionary& viscosityProperties);
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace viscosityModels
} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
